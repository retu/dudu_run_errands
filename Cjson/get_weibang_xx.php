<?php
require('config.php');
	function getDistance($lat1, $lng1, $lat2, $lng2, $len_type = 1, $decimal = 2)
	{ 
	 	$EARTH_RADIUS=6378.137; 
	 	$PI=3.1415926; 
	 	$radLat1 = $lat1 * $PI / 180.0;
	 	$radLat2 = $lat2 * $PI / 180.0;
	 	$a = $radLat1 - $radLat2; 
	 	$b = ($lng1 * $PI / 180.0) - ($lng2 * $PI / 180.0);
	 	$s = 2 * asin(sqrt(pow(sin($a/2),2) + cos($radLat1) * cos($radLat2) * pow(sin($b/2),2)));
	 	$s = $s * $EARTH_RADIUS; 
	 	$s = round($s * 1000); 
	 	if ($len_type > 1) 
	 	{ 
	 		$s /= 1000; 
	 	} 
	 	return round($s,$decimal); 
	}
		$wb_id=$_REQUEST['wb_id'];
		$lat=$_REQUEST['lat'];
		$lon=$_REQUEST['lon'];
		$user_id='0';
		if(isset($_REQUEST['user_id'])) $user_id=$_REQUEST['user_id'];
		$sql="select ".$oto."_help.*,".$oto."_users.userName,".$oto."_users.userPhoto,".$oto."_users.grage,".$oto."_users.userBond,".$oto."_users.isBondShow,".$oto."_users.userPhone from `".$oto."_help`,".$oto."_users where id='{$wb_id}' and isDel='0' and ".$oto."_users.userId=".$oto."_help.userId ";
		$result=$db->query($sql);
		while($row=$result->fetch_assoc()){
			$help[]=$row;//将取得的所有数据赋值给person_info数组
		}
		$sql="select * from ".$oto."_help_order_user where help_id='{$wb_id}' and user_id='{$user_id}' and isDel='0'";
		$result=$db->query($sql);
		while($row=$result->fetch_assoc()){
			$isOrderUser[]=$row;//将取得的所有数据赋值给person_info数组
		}
		if(isset($isOrderUser))
		{
			$help[0]['isGo']=-1;
		}
		else $help[0]['isGo']=1;
		$sql="select * from ".$oto."_help_orders where helpId='{$wb_id}' and isDel='0' and orderStatus not in ('-1','-2','-5','-6')";
		$result=$db->query($sql);
		while($row=$result->fetch_assoc()){
			$isOrder[]=$row;//将取得的所有数据赋值给person_info数组
		}
		if(isset($help))
		{
			for($i=0;$i<count($help);$i++)
			{
				$lat2=$help[$i]['lat'];
				$lon2=$help[$i]['lon'];
				$wb_id=$help[$i]['id'];
				$cid=$help[$i]['cid'];
				$sql="select * from `".$oto."_help_cate` where `id`='{$cid}'";
				$result=$db->query($sql);
				while($row=$result->fetch_assoc()){
					$help_cate[$i][]=$row;//将取得的所有数据赋值给person_info数组
				}
				$sql="select * from `".$oto."_help_order_user` where `help_id`='{$wb_id}' and isDel='0'";
				$result=$db->query($sql);
				while($row=$result->fetch_assoc()){
					$order_user[$i][]=$row;//将取得的所有数据赋值给person_info数组
				}
				if(isset($help_cate[$i]))
				{
					$help[$i]['leixing']=$help_cate[$i][0]['cateName'];
				}
				else $help[$i]['leixing']="";
				if(isset($order_user[$i]))
				{
					$help[$i]['jiedan_num']=count($order_user[$i]);
				}
				else $help[$i]['jiedan_num']=0;
				$help[$i]['img_list']=explode("|",$help[$i]['img']);
				$help[$i]['juli']=getDistance($lat,$lon,$lat2,$lon2);
				$fanwei=$help[$i]['range']*1000;
				$help[$i]['fanwei_pan']=1;
				if($help[$i]['juli']>$fanwei)
				{
					$help[$i]['fanwei_pan']=0;
					// array_splice($help, $i, 1);
					// $i--;
				}
				if($help[$i]['range']==0 || $help[$i]['range']=='0') $help[$i]['fanwei_pan']=1;
			}
			if(isset($isOrder))
			{
				if($help[0]['isGo']==1) $help[0]['isGo']=0;
			}
			echo json_encode($help);
		}
		else echo 'null';
?>