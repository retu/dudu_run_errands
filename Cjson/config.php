<?php
/*
 * 配置连接数据库信息
 */
//$pz=include"../system/modules/member/lib/user_fufen.ini.php";
header('Content-type:text/html;charset=utf-8');
header('Access-Control-Allow-Origin: *');
$host='127.0.0.1';//主机
$user='user';//数据库账号
$password='EDC4rfv';//数据库密码

$database='o2obase';//数据库名

$WEB_PATH='http://hdo2o.***.com';//服务器域名

$WEB_PATH= 'http://192.168.1.225:1999';
//打开数据库连接
$db = mysqli_connect($host, $user, $password, $database);
//判断连接是否成功
if ($db) {
    $db->query("set names utf8"); //设置UTF-8编码(JSON的唯一编码)
} else {
    echo 'DATABASE_CONNECTION_DIE'; //数据库连接失败
    exit;
}
$oto = 'oto';
$pt = 'pt';
$isEmotion = 1; //是否开启表情转换功能

/**
 * @return mysqli
 */
function ptconnect()
{
    $pthost = '127.0.0.1'; //主机
    

    $ptuser = 'user'; //数据库账号
    $ptpassword = 'EDC4rfv'; //数据库密码
    $ptdatabase = 'paotuibase'; //数据库名
    
    $ptdb = mysqli_connect($pthost, $ptuser, $ptpassword, $ptdatabase);
    if ($ptdb) {
        $ptdb->query("set names utf8"); //设置UTF-8编码(JSON的唯一编码)
    } else {
        echo 'DATABASE_CONNECTION_DIE'; //数据库连接失败
        exit;
    }
    return $ptdb;
}

//跑腿订单修改
function changePtOrder($orderId, $type)
{
    $ptdb = ptconnect();
    $pt_time = time();
    if ($type == 1) {
        $sql = "UPDATE `pt_delivery_order` SET `status` = 3,`confirm_time` = {$pt_time} WHERE orderId = {$orderId}";
    } else if ($type == 2) {
        $sql = "UPDATE `pt_delivery_order` SET `iscancel` = 1,`cancel_time` = {$pt_time} WHERE orderId = {$orderId}";
    }
    $ptdb->query($sql);
    mysqli_close($ptdb);
}

//获取平台配送设置
function getPtDeliveryConfig(){
    $ptdb = ptconnect();
    $sql = "SELECT `value` FROM`pt_system` WHERE `name` = 'delivery_config' LIMIT 1";
    $result = $ptdb->query($sql);
   
    if ($result){
    	$value = array();
	    while ($row = $result->fetch_assoc()) {
	        $value = $row['value'];
	    }
	    return unserialize($value);
    }
    return '';
    
}

//高德坐标转百度坐标
function gcjBd($lat, $lng)
{
    $x_pi = 3.14159265358979324 * 3000.0 / 180.0;
    $pi = 3.14159265358979324; // 圆周率
    $a = 6378245.0; // WGS 长轴半径
    $ee = 0.00669342162296594323; // WGS 偏心率的平方
    $x = $lat;
    $y = $lng;
    $z = sqrt($x * $x + $y * $y) + 0.00002 * sin($y * $x_pi);
    $theta = atan2($y, $x) + 0.000003 * cos($x * $x_pi);
    $bd_x = $z * cos($theta) + 0.0065;
    $bd_y = $z * sin($theta) + 0.006;
    $data['lat'] = $bd_x;
    $data['lng'] = $bd_y;
    return $data;
}

?>