<?php
require('config.php');
	function getDistance($lat1, $lng1, $lat2, $lng2, $len_type = 1, $decimal = 2)
	{ 
	 	$EARTH_RADIUS=6378.137; 
	 	$PI=3.1415926; 
	 	$radLat1 = $lat1 * $PI / 180.0;
	 	$radLat2 = $lat2 * $PI / 180.0;
	 	$a = $radLat1 - $radLat2; 
	 	$b = ($lng1 * $PI / 180.0) - ($lng2 * $PI / 180.0);
	 	$s = 2 * asin(sqrt(pow(sin($a/2),2) + cos($radLat1) * cos($radLat2) * pow(sin($b/2),2)));
	 	$s = $s * $EARTH_RADIUS; 
	 	$s = round($s * 1000); 
	 	if ($len_type > 1) 
	 	{ 
	 		$s /= 1000; 
	 	} 
	 	return round($s,$decimal); 
	}

        $lat=$_REQUEST['lat'];
        $lon=$_REQUEST['lon'];
        $sql="select * from ".$oto."_sys_configs where fieldName='店铺附近范围'";
        $result=$db->query($sql);
		while($row=$result->fetch_assoc()){
			$sys_configs[]=$row;//将取得的所有数据赋值给person_info数组
		}
		if(isset($sys_configs))
		{
			$fanwei=$sys_configs[0]['fieldCode'];
		}
		$sql="select * from `".$oto."_shops` where `shopFlag`='1' and isHot='1' and `shopStatus`='1'";
		$result=$db->query($sql);
		while($row=$result->fetch_assoc()){
			$shop_info[]=$row;//将取得的所有数据赋值给person_info数组
		}
		if(isset($shop_info))
		{
			for($i=0;$i<count($shop_info);$i++)
			{
				$lat2=$shop_info[$i]['latitude'];
				$lon2=$shop_info[$i]['longitude'];
				$shop_info[$i]['juli']=getDistance($lat,$lon,$lat2,$lon2);
				if($shop_info[$i]['juli']>$fanwei)
				{
					array_splice($shop_info, $i, 1);
					$i--;
				}
			}
				for($i=0;$i<count($shop_info);$i++)
				{
					for($j=$i;$j<count($shop_info);$j++)
					{
						if($shop_info[$j]['juli']<$shop_info[$i]['juli'])
						{
							$t=$shop_info[$i];
							$shop_info[$i]=$shop_info[$j];
							$shop_info[$j]=$t;
						}
					}
				}
			echo json_encode($shop_info);
		}
		else
		{
			echo 'null';
		}
?>