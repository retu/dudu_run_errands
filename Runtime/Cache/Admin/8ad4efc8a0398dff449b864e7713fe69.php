<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html lang="zh-cn">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <title><?php echo ($CONF['mallTitle']); ?>后台管理中心</title>
      <link href="/Public/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet">
      <link href="/Tpl/Admin/css/AdminLTE.css" rel="stylesheet" type="text/css" />
      <link href="/Public/plugins/jedate/skin/jedate.css" rel="stylesheet">
      <!--[if lt IE 9]>
      <script src="/Public/js/html5shiv.min.js"></script>
      <script src="/Public/js/respond.min.js"></script>
      <![endif]-->
      <script src="/Public/js/jquery.min.js"></script>
      <script src="/Public/plugins/bootstrap/js/bootstrap.min.js"></script>
      <script src="/Public/js/common.js"></script>
      <script src="/Public/plugins/plugins/plugins.js"></script>
    <script src="/Public/plugins/jedate/jquery.jedate.min.js"></script>
   </head>
   <script>
   function del(id){
	   var msg = '您确定要删除吗?';
	   Plugins.confirm({title:'信息提示',content:msg,okText:'确定',cancelText:'取消',okFun:function(){
		   Plugins.closeWindow();
		   Plugins.waitTips({title:'信息提示',content:'正在操作，请稍后...'});
		   $.post("<?php echo U('deleteArea');?>",{id:id},function(data,textStatus){
					var json = WST.toJson(data);
					if(json.status=='1'){
						Plugins.setWaitTipsMsg({content:'操作成功',timeout:1000,callback:function(){
						   location.reload();
						}});
					}else{
						Plugins.closeWindow();
						Plugins.Tips({title:'信息提示',icon:'error',content:'操作失败!',timeout:1000});
					}
				});
	   }});
   }
   </script>
   <body class='wst-page'>
    <form method='get' action='<?php echo U("index");?>'>
    <input type="hidden" value="Admin" name="m"/>
    <input type="hidden" value="Pagent" name="c"/>
    <input type="hidden" value="minMoney" name="a"/>
       <?php if(isset($edit)): ?><div class='wst-tbar'>
	       提现最低金额：<input type='text' id='minMoney' name='minMoney' class='form-control wst-ipt-10' value='<?php echo ($minMoney); ?>'/>
	  <button type="submit" class="btn btn-primary glyphicon glyphicon-search">编辑</button> 
       </div><?php endif; ?>
   </form>
       
       <div class="wst-body">
        <table class="table table-hover table-striped table-bordered wst-list">
           <thead>
             <tr>
               <th width='50'>用户名</th>
               <th width='60'>申请类型</th>
               <th width="60">申请金额</th>
               <th width='60'>申请时间</th>
               <th width='40'>状态</th>
               <th width='40'>账户详情</th>
               <th width='100'>操作</th>
             </tr>
           </thead>
           <tbody>
            <?php if(is_array($lists)): $i = 0; $__LIST__ = $lists;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?><tr>
               <td><?php echo ($vo["id"]); ?></td>
               <td><?php echo ($vo['loginName']); ?></td>
               <td><?php echo ($vo['trueName']); ?></td>
               <td><?php echo ($vo['balance']); ?></td>
               <td><?php echo ($vo['province_name']); echo ($vo['city_name']); echo ($vo['area_name']); ?></td>
               <td><?php echo (date('Y-m-d H:i:s',$vo['time'])); ?></td>
               <td>
               <?php if(in_array('hylb_02',$WST_STAFF['grant'])){ ?>
               <a class="btn btn-default" href="<?php echo U('infolist',array('id'=>$vo['id']));?>">代理详情</a>&nbsp;
               <?php } ?>
               <?php if(in_array('hylb_02',$WST_STAFF['grant'])){ ?>
               <a class="btn btn-default glyphicon glyphicon-pencil" href="<?php echo U('toEdit',array('id'=>$vo['id']));?>">修改</a>&nbsp;
               <?php } ?>
               <?php if(in_array('hylb_03',$WST_STAFF['grant'])){ ?>
               <button type="button" class="btn btn-default glyphicon glyphicon-trash" onclick="javascript:del(<?php echo ($vo['id']); ?>)">刪除</buttona>&nbsp;
               <?php } ?>
               </td>
             </tr><?php endforeach; endif; else: echo "" ;endif; ?>
             <tr>
                <td colspan='11' align='center'><?php echo ($page); ?></td>
             </tr>
           </tbody>
        </table>
       </div>
<script>
$(function(){
	$('#province').change(function(){
		var province_id  = $(this).val();
		$.ajax({
			type:'get',
			url:'<?php echo U('get_child_address');?>',
			dataType:'json',
			data : {pid:province_id},
			success:function(data){
				var data = '<option value="0">请选择城市</option>'+data;
				$('#city').html(data);
				var data = '<option value="0">请选择区域</option>';
				$('#area').html(data);
			},
		});
	});
	$('#city').change(function(){
		var city_id = $(this).val();
		$.ajax({
			type:'get',
			url:'<?php echo U('get_child_address');?>',
			dataType:'json',
			data : {pid:city_id},
			success:function(data){
				var data = '<option value="0">请选择区域</option>'+data;
				$('#area').html(data);
			},
		});
	});
	
        var startMonth = {
            format: 'YYYY-MM-DD'
        };
        var endMonth = {
            format: 'YYYY-MM-DD'
        };
        $("#startDate").jeDate(startMonth);
        $("#endDate").jeDate(endMonth);
	
});
</script>   
</body>
</html>