<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html lang="zh-cn">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <title><?php echo ($CONF['shopTitle']['fieldValue']); ?>后台管理中心</title>
      <link href="/Public/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet">
      <link href="/Tpl/Admin/css/AdminLTE.css" rel="stylesheet" type="text/css" />
      <link href="/Tpl/Admin/css/upload.css" rel="stylesheet" type="text/css" />
      
      <!--[if lt IE 9]>
      <script src="/Public/js/html5shiv.min.js"></script>
      <script src="/Public/js/respond.min.js"></script>
      <![endif]-->
      <script src="/Public/js/jquery.min.js"></script>
      <script src="/Public/plugins/bootstrap/js/bootstrap.min.js"></script>
      <script src="/Public/js/common.js"></script>
      <script src="/Public/plugins/plugins/plugins.js"></script>
      
      <script src="/Public/plugins/formValidator/formValidator-4.1.3.js"></script>
      <script type="text/javascript" src="/Tpl/Admin/js/upload.js"></script>
      
   </head>
   <script>
   var ThinkPHP = window.Think = {
	        "ROOT"   : ""
	}
   $(function () {
	   $.formValidator.initConfig({
		   theme:'Default',mode:'AutoTip',formID:"myform",debug:true,submitOnce:true,onSuccess:function(){
				   edit();
			       return false;
			}});
		$("#trueName").formValidator({
			onShow:"",onFocus:"请输入真实姓名"
		}).inputValidator({
			min:2,max:50,onError:"请输入真实姓名"
		});
		<?php if(!isset($object)): ?>$("#loginPwd").formValidator({
				onShow:"",onFocus:"登录密码长度应该为6-20位之间"
			}).inputValidator({
				min:6,max:50,onError:"登录密码长度应该为6-20位之间"
			});<?php endif; ?>
		
		<?php if(isset($object)): ?>$("#loginPwd").formValidator({
				onShow:"",onFocus:"登录密码长度应该为6-20位之间"
			});
			$("#loginPwd").blur(function(){
				  if($("#loginPwd").val()==''){
					  $("#loginPwd").unFormValidator(true);
				  }else{
					  $("#loginPwd").unFormValidator(false);
				  }
			});<?php endif; ?>
		
		$("#loginName").formValidator({
			onShow:"",onFocus:"请输入账号或手机号码"
		}).inputValidator({
			min:2,max:50,onError:"请输入用户名或手机号码"
		});	
		$("#area").formValidator({
			onShow:"",onFocus:"请选择负责区域"
		}).inputValidator({
			min:1,max:50,onError:"请选择负责区域"
		});
   });
   function edit(){
	   var params = {};
	   params.trueName = $.trim($('#trueName').val());
	   params.loginPwd = $.trim($('#loginPwd').val());
	   params.loginName = $.trim($('#loginName').val());
	   params.province = $.trim($('#province').val());
	   params.city = $.trim($('#city').val());
	   params.area = $.trim($('#area').val());
	   params.id = $('#id').val();
	   Plugins.waitTips({title:'信息提示',content:'正在提交数据，请稍后...'});
		$.post("<?php echo U('edit');?>",params,function(data,textStatus){
			var json = WST.toJson(data);
			if(json.status=='1'){
				Plugins.setWaitTipsMsg({ content:'操作成功',timeout:1000,callback:function(){
				   location.href='<?php echo U("index");?>';
				}});
			}else{
				Plugins.closeWindow();
				Plugins.Tips({title:'信息提示',icon:'error',content:json.msg,timeout:1000});
			}
		});
   }
  
   </script>
   <body class="wst-page">
       <form name="myform" method="post" id="myform" autocomplete="off">   
        <input type='hidden' id='id' value='<?php echo ($object["id"]); ?>'/>
       
        <table class="table table-hover table-striped table-bordered wst-form">
           <tr>
             <th width='120' align='right'>真实姓名<font color='red'>*</font>：</th>
             <td><input type='text' id='trueName' name='trueName' class="form-control wst-ipt" value='<?php echo ($object["trueName"]); ?>' maxLength='20'/></td>
           </tr>
           <tr>
             <th width='120' align='right'>账号<font color='red'>*</font>：</th>
             <td><input type='text' id='loginName' name='loginName' class="form-control wst-ipt" value='<?php echo ($object["loginName"]); ?>' maxLength='20'/></td>
           </tr>
           <tr>
             <th width='120' align='right'>密码<font color='red'>*</font>：</th>
             <td>
             <input type='password' id='loginPwd' class="form-control wst-ipt" maxLength='20'/>
             <?php if($object['id'] !=0 ): ?>(为空则说明不修改密码)<?php endif; ?></td>
           </tr>
			<tr>
			 <th width='120' align='right'>负责区域<font color='red'>*</font>：</th>
			 <td>
			 	<select name="province" id="province">
			 		<option value="0">请选择省份</option>
			 		<?php if(is_array($province)): foreach($province as $key=>$v): ?><option value="<?php echo ($v["id"]); ?>" <?php if($v['id'] == $object['province']): ?>selected="selected"<?php endif; ?>><?php echo ($v["name"]); ?></option><?php endforeach; endif; ?>
			 	</select>
			 	<select name="city" id="city">
			 		<option value="0">请选择城市</option>
			 					 		<?php if(is_array($city)): foreach($city as $key=>$v): ?><option value="<?php echo ($v["id"]); ?>" <?php if($v['id'] == $object['city']): ?>selected="selected"<?php endif; ?>><?php echo ($v["name"]); ?></option><?php endforeach; endif; ?>
			 	</select>
			 	<select name="area" id="area">
			 		<option value="0">请选择区域</option>
			 					 		<?php if(is_array($area)): foreach($area as $key=>$v): ?><option value="<?php echo ($v["id"]); ?>" <?php if($v['id'] == $object['area']): ?>selected="selected"<?php endif; ?>><?php echo ($v["name"]); ?></option><?php endforeach; endif; ?>
			 	</select>
			 </td>
			</tr>
           <tr>
             <td colspan='3' style='padding-left:250px;'>
                 <button type="submit" class="btn btn-success">保&nbsp;存</button>
                 <button type="button" class="btn btn-primary" onclick='javascript:location.href="<?php echo U('index');?>"'>返&nbsp;回</button>
             </td>
           </tr>
        </table>
       </form>
<script>
$(function(){
	$('#province').change(function(){
		var province_id  = $(this).val();
		$.ajax({
			type:'get',
			url:'<?php echo U('get_child_address');?>',
			dataType:'json',
			data : {pid:province_id},
			success:function(data){
				var data = '<option value="0">请选择城市</option>'+data;
				$('#city').html(data);
				var data = '<option value="0">请选择区域</option>';
				$('#area').html(data);
			},
		});
	});
	$('#city').change(function(){
		var city_id = $(this).val();
		$.ajax({
			type:'get',
			url:'<?php echo U('get_child_address');?>',
			dataType:'json',
			data : {pid:city_id},
			success:function(data){
				var data = '<option value="0">请选择区域</option>'+data;
				$('#area').html(data);
			},
		});
	});
});
</script>
</body>
</html>