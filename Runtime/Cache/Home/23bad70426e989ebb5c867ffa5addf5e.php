<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>首页 - <?php echo ($CONF['mallTitle']); ?></title>
<meta name="keywords" content="<?php echo ($CONF['mallKeywords']); ?>" />
<meta name="description" content="<?php echo ($CONF['mallDesc']); ?>" />
<link rel="stylesheet" href="/Tpl/default/css/common.css" />
<link rel="stylesheet" href="/Tpl/default/css/base.css" />
<link rel="stylesheet" href="/Tpl/default/css/head.css" />
<link rel="stylesheet" href="/Tpl/default/css/Comm.css"/>
<link rel="stylesheet" href="/Tpl/default/css/List.css"/>
    <link rel="stylesheet" href="/Tpl/default/css/magnifier.css" />
<script src="/Tpl/default/js/jquery-1.8.3.min.js"></script>

<link rel="stylesheet" href="/Tpl/default/css/regist.css">
<link rel="stylesheet" href="/Tpl/default/css/passport.css">
<link rel="stylesheet" href="/Tpl/default/css/footer.css">
	<style>
.div1{ background: #E23C3D none repeat scroll 0 0;border: 1px solid red;border-radius: 2px;float: left;height: 35px;position: relative;top: 0px;width: 45px; }
.div1:hover{background: none repeat scroll 0 0 red;border: 1px solid red;border-radius: 2px;float: left;height: 35px;position: relative;top: 0px;width: 45px;}
.div2{font-size: 13px;line-height: 35px;text-align: center;}
.inputstyle{cursor: pointer;font-size: 13px;height: 35px;left: 0px;opacity: 0;outline: medium none;position: absolute;top: 0px;width: 45px;}
</style>
	<script>
	var filetypes = ["gif","jpg","png","jpeg"];
	function getCatListForEdit(objId,parentId,t,id){
     var params = {};
     params.id = parentId;
     $('#'+objId).empty();
     if(t<1){
       $('#goodsCatId3').empty();
       $('#goodsCatId3').html('<option value="">请选择</option>');
       getBrands(parentId);
     }
     var html = [];
     $.post(Think.U('Home/GoodsCats/queryByList'),params,function(data,textStatus){
        html.push('<option value="">请选择</option>');
      var json = WST.toJson(data);
      if(json.status=='1' && json.list){
        var opts = null;
        for(var i=0;i<json.list.length;i++){
          opts = json.list[i];
          html.push('<option value="'+opts.catId+'" '+((id==opts.catId)?'selected':'')+'>'+opts.catName+'</option>');
        }
      }
      $('#'+objId).html(html.join(''));
     });
	}
	function getBrands(catId){
	  var v = $('#brandId').attr('dataVal');
	  var params = {};
	  params.catId = catId;
	  $('#brandId').empty();
	  var html = [];
	  $('#brandId').append('<option value="0">请选择</option>');
	  $.post(Think.U('Home/Brands/queryBrandsByCat'),params,function(data,textStatus){
	    var json = WST.toJson(data);
	    if(json.status=='1' && json.list){
	      for(var i=0;i<json.list.length;i++){
	        opts = json.list[i];
	        $('#brandId').append('<option value="'+opts.brandId+'" '+((v==opts.brandId)?'selected':'')+'>'+opts.brandName+'</option>');
	      }
	    }
	  })
	}
	</script>

</head>
<body>
<script src="/Public/js/jquery.min.js"></script>
<script src="/Public/plugins/lazyload/jquery.lazyload.min.js?v=1.9.1"></script>
<script type="text/javascript">
var img0='/Tpl/default/images';
var ThinkPHP = window.Think = {
        "ROOT"   : "",
        "APP"    : "",
        "PUBLIC" : "/Public",
        "DEEP"   : "<?php echo C('URL_PATHINFO_DEPR');?>",
        "MODEL"  : ["<?php echo C('URL_MODEL');?>", "<?php echo C('URL_CASE_INSENSITIVE');?>", "<?php echo C('URL_HTML_SUFFIX');?>"],
        "VAR"    : ["<?php echo C('VAR_MODULE');?>", "<?php echo C('VAR_CONTROLLER');?>", "<?php echo C('VAR_ACTION');?>"]
}
    var domainURL = "<?php echo WSTDomain();?>";
    var publicurl = "/Public";
    var currCityId = "<?php echo ($currArea['areaId']); ?>";
    var currCityName = "<?php echo ($currArea['areaName']); ?>";
    var currDefaultImg = "<?php echo WSTDomain();?>/<?php echo ($CONF['goodsImg']); ?>";
    var wstMallName = "<?php echo ($CONF['mallName']); ?>";
    $(function() {
    	$('.lazyImg').lazyload({ effect: "fadeIn",failurelimit : 10,threshold: 200,placeholder:currDefaultImg});
    	$("#app-jd").hover(function(){
			$("#apps").show();
			$(".app-jd img").addClass("img180");
		},function(){
			$("#apps").hide();
			$(".app-jd img").removeClass("img180");	
		})
    });
</script>
<script src="/Public/js/think.js"></script>
<div id="wst-shortcut">
	<div class="w">
		<ul class="fl lh">
			<li class="fore1 ld"><b></b><a href="javascript:addToFavorite()"
				rel="nofollow">收藏<?php echo ($CONF['mallName']); ?></a></li><s></s>
			<li class="fore3 ld menu" id="app-jd" data-widget="dropdown">
				<span class="outline"></span> <span class="blank"></span> 
				<a href="#" target="_blank"><img src="/Tpl/default/images/icon_top_02.png"/>&nbsp;<?php echo ($CONF['mallName']); ?> 手机版</a>
					<table id="apps" style="border:1px solid #eee;background-color: white;position:absolute;margin-left: -13px;border-spacing: 10px 5px;border-collapse:separate;display: none">
						<tr>
							<td><img src="/<?php echo ($CONF['iPhone']); ?>"  width="90" height="90"/></td>
							<td><img src="/<?php echo ($CONF['Android']); ?>" width="90" height="90"></td>
						</tr>
						<tr>
							<td style="text-align: center;" >iPhone</td>
							<td style="text-align: center;" >Android</td>
						</tr>
					</table>
			</li>
			<li class="fore4" id="biz-service" data-widget="dropdown" style='padding:0;'>&nbsp;<s></s>&nbsp;&nbsp;&nbsp;
				所在城市
				【<span class='wst-city'>&nbsp;<?php echo ($currArea["areaName"]); ?>&nbsp;</span>】
				<img src="/Tpl/default/images/icon_top_03.png"/>	
				&nbsp;&nbsp;<a href="javascript:;" onclick="toChangeCity();">切换城市</a><i class="triangle"></i>
			</li>
		</ul>
	
		<ul class="fr lh" style='float:right;'>
			<li class="fore1" id="loginbar" style="width: 320px;text-align: right;" ><a href="<?php echo U('Home/Orders/queryByPage');?>"><span style='color:blue'><?php echo ($WST_USER['loginName']); ?></span></a> 欢迎您来到 <a href='<?php echo WSTDomain();?>'><?php echo ($CONF['mallName']); ?></a>！<s></s>&nbsp;
			<span>
				<?php if(!$WST_USER['userId']): ?><a href="<?php echo U('Home/Users/login');?>">[登录]</a>
				<a href="<?php echo U('Home/Users/regist');?>"	class="link-regist">[免费注册]</a><?php endif; ?>
				<?php if($WST_USER['userId'] > 0): ?><a href="javascript:logout();">[退出]</a><?php endif; ?>
			</span>
			</li>
			<li class="fore2 ld"><s></s>
			<?php if(session('WST_USER.userId')>0){ ?>
				<?php if(session('WST_USER.userType')==0){ ?>
				    <a href="<?php echo U('Home/Shops/toOpenShopByUser');?>" rel="nofollow">我要开店</a>
				<?php }else{ ?>
				    <?php if(session('WST_USER.loginTarget')==0){ ?>
				        <a href="<?php echo U('Home/Shops/index');?>" rel="nofollow">卖家中心</a>
				    <?php }else{ ?>
				        <a href="<?php echo U('Home/Users/index');?>" rel="nofollow">买家中心</a>
				    <?php } ?>
				<?php } ?>
			<?php }else{ ?>
			    <a href="<?php echo U('Home/Shops/toOpenShop');?>" rel="nofollow">我要开店</a>
			<?php } ?>
			</li>
		</ul>
		<span class="clr"></span>
	</div>
</div>



<div style="height:132px;">
<div id="mainsearchbox" style="text-align:center;">
	<div id="wst-search-pbox">
		<div style="float:left;width:240px;" class="wst-header-car">
		  <a href='<?php echo WSTDomain();?>'>
			<img id="wst-logo" style="margin-top:30px;margin-left:35px;width: 178px;height: 60px;
" src="<?php echo WSTDomain();?>/<?php echo ($CONF['mallLogo']); ?>"/>
		  </a>	
		</div>
		<div id="wst-search-container">
			<div id="wst-search-type-box">
				<input id="wst-search-type" type="hidden" value="<?php echo ($searchType); ?>"/>
				<div id="wst-panel-goods" class="<?php if($searchType == 1): ?>wst-panel-curr<?php else: ?>wst-panel-notcurr<?php endif; ?>">商品</div>
				<div id="wst-panel-shop" class="<?php if($searchType == 2): ?>wst-panel-curr<?php else: ?>wst-panel-notcurr<?php endif; ?>">店铺</div>
				<div class="wst-clear"></div>
			</div>
			<div id="wst-searchbox">
				<input id="keyword" class="wst-search-keyword" data="wst_key_search" onkeyup="getSearchInfo(this,event);" placeholder="<?php if($searchType == 2): ?>搜索 店铺<?php else: ?>搜索 商品<?php endif; ?>" autocomplete="off"  value="<?php echo ($keyWords); ?>">
				<div id="btnsch" class="wst-search-button"></div>
				<div id="wst_key_search_list" style="position:absolute;top:38px;left:-1px;border:1px solid #b8b8b8;min-width:567px;display:none;background-color:#ffffff;z-index:1000;">dfdf</div>
			</div>
			<div id="wst-hotsearch-keys">
			</div>
		</div>
		<div id="wst-search-des-container">
			<div class="des-box">
				<div class='wst-reach'>
					<img src="/Tpl/default/images/sadn.png"  height="24" width="32" style="margin-top: 5px;" />
					<div style="float:left;position:absolute;top:0px;left:38px;"><span style="font-weight:bolder;">闪电配送</span><br/><span style="color:#fe3689;">最快1小时送达</span></div>
				</div>
				<div class='wst-since'>
					<img src="/Tpl/default/images/sqzt.png"  height="32" width="32" style="margin-top: 2px;"/>
					<div style="float:left;position:absolute;top:0px;left:38px;"><span style="font-weight:bolder;">社区自提</span><br/><span style="color:#fe3689;">330家自提点</span></div>
				</div>
			</div>
		</div>
	</div>
</div>
</div>
<div class="headNav">
		  <div class="navCon w1020">
		  	<div class="wst-slide-controls">
		  		<?php if(is_array($indexAds)): $k = 0; $__LIST__ = $indexAds;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($k % 2 );++$k; if($k == 1): ?><span class="curr"><!-- <?php echo ($k); ?> --></span>
		  		  	<?php else: ?>
		  		    	<span class=""><!-- <?php echo ($k); ?> --></span><?php endif; endforeach; endif; else: echo "" ;endif; ?>
			</div>
		    <div class="navCon-cate fl navCon_on" >
		      <div class="navCon-cate-title"> <a href="<?php echo U('Home/goods/getGoodsList');?>">全部商品分类<img src="/Tpl/default/images/jiantou.png"></a></div>
		      
		      	<?php if($ishome == 1): ?><div class="cateMenu1" >
		      	<?php else: ?>
		      		<!--
		      		<div class="cateMenu2" style="display:none;" >
		      		-->
		      		<div class="cateMenu1" ><?php endif; ?>
		        <div id="wst-nvg-cat-box" style="position:relative; display: none;">
		        	<div class="wst-nvgbk" style="diplay:none;"></div>
		        	<?php $_result=WSTGoodsCats();if(is_array($_result)): $k1 = 0; $__LIST__ = $_result;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo1): $mod = ($k1 % 2 );++$k1; if($k1 < 7): ?><li class="wst-nvg-cat-nlt6" style="border-top: none;" >
				    	<?php else: ?>
				    	<li class="wst-nvg-cat-gt6" style="border-top: none;display:none;" ><?php endif; ?>
				    	<div>
				            <div class="cate-tag"> 
				            <div class="listModel ">
				             <p > 
				            	<strong><s<?php echo ($k1); ?>></s<?php echo ($k1); ?>>&nbsp;<a style="font-weight:bold;" href="<?php echo U('Home/goods/getGoodsList',array('c1Id'=>$vo1['catId']));?>"><?php echo ($vo1["catName"]); ?></a></strong>
				             </p> 
				             </div>  
				             <div class="listModel">
				                <p> 
				                <?php if(is_array($vo1['catChildren'])): $k2 = 0; $__LIST__ = $vo1['catChildren'];if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo2): $mod = ($k2 % 2 );++$k2;?><a href="<?php echo U('Home/goods/getGoodsList',array('c1Id'=>$vo1['catId'],'c2Id'=>$vo2['catId']));?>"><?php echo ($vo2["catName"]); ?></a><?php endforeach; endif; else: echo "" ;endif; ?>
				                </p>
				              </div>
				            </div>
				            <div class="list-item hide">
				              <ul class="itemleft">
				              	<?php if(is_array($vo1['catChildren'])): $k2 = 0; $__LIST__ = $vo1['catChildren'];if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo2): $mod = ($k2 % 2 );++$k2;?><dl>
				                  <dt><a href="<?php echo U('Home/goods/getGoodsList',array('c1Id'=>$vo1['catId'],'c2Id'=>$vo2['catId']));?>"><?php echo ($vo2["catName"]); ?></a></dt>
				                  <dd> 
				                  <?php if(is_array($vo2['catChildren'])): $k3 = 0; $__LIST__ = $vo2['catChildren'];if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo3): $mod = ($k3 % 2 );++$k3;?><a href="<?php echo U('Home/goods/getGoodsList',array('c1Id'=>$vo1['catId'],'c2Id'=>$vo2['catId'],'c3Id'=>$vo3['catId']));?>"><?php echo ($vo3["catName"]); ?></a><?php endforeach; endif; else: echo "" ;endif; ?>
				                  </dd>
				                </dl>
				                <div class="fn-clear"></div><?php endforeach; endif; else: echo "" ;endif; ?>
				              </ul>
				            </div>
				            </div>
				  		</li><?php endforeach; endif; else: echo "" ;endif; ?>
		          	
		          	<li style="display:none;"></li>
		        </div>
		      </div>
		    </div>
		    
		    <div class="navCon-menu fl">
		      <ul class="fl" style="background-color: #fe3689;width:880px;">
		        <?php $_result=WSTNavigation(0);if(is_array($_result)): $i = 0; $__LIST__ = $_result;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?><li ><a href="<?php echo ($vo['navUrl']); ?>" <?php if($vo['isOpen'] == 1): ?>target="_blank"<?php endif; ?> <?php if($vo['active'] == 1): ?>class="curMenu"<?php endif; ?>>&nbsp;&nbsp;<?php echo ($vo['navTitle']); ?>&nbsp;&nbsp;</a></li><?php endforeach; endif; else: echo "" ;endif; ?>
		      </ul>
		    </div>
		    <li id="wst-nvg-cart">
		    	<div class='wst-nvg-cart-cost'>
		       		&nbsp;<span class="wst-nvg-cart-cnt">购物车0</span>件&nbsp;|&nbsp;<span class="wst-nvg-cart-price">0.00</span>￥
		       	</div>
			</li>
			<div class="wst-cart-box"><div class="wst-nvg-cart-goods">购物车中暂无商品</div></div>
		  </div>
		</div>
		<script>
		$(function(){
			checkCart();
		});
		</script>

		
<div class="container">
	
    <style>
    .yj-select select{height:30px;font-size:14px;margin-right:5px;}
    .ATRoot{padding:0px 5px;height:22px;line-height:22px;clear:both;}
    .ATNode{margin-right:5px;;line-height:22px;width:150px;float:left;margin-top:2px;}
    label{cursor:pointer;}
    .tleft{float:left;}
    .Hide{display:none;}
    dl.areaSelect{display: inline-block; width:100%; margin-bottom: 0;}
    dl.areaSelect:hover{border:1px solid #E5CD29;}
    dl.areaSelect:hover dd{display: block;}
    dl.areaSelect dd{margin-left:20px;width:100%;}
    </style>
	<div class="wst-regist" id="regist">	    
	    <div class="mc" style="position:relative;">
	        <div style="font-size:30px;color:black;padding:15px 0px 15px 355px;border-bottom:dotted 1px #ddd; ">店铺注册</div>
	    	<iframe name="upload" style="display:none"></iframe>
	    	<!--店铺图标-->
			<form id="uploadform_Filedata" autocomplete="off" style="position:absolute;top:640px;left:341px;z-index:10;"enctype="multipart/form-data" method="POST" target="upload" action="<?php echo U('Home/Shops/uploadPic');?>" >
				<div style="position:relative;">
				<input id="shopImg" name="shopImg" disabled="disabled" class="text" type="text" value="<?php echo ($object["shopImg"]); ?>" readonly style="margin-right:4px;float:left;margin-left:8px;width:237px;"/>
				<div class="div1">
					<div class="div2">浏览</div>
					<input type="file" class="inputstyle" id="Filedata" name="Filedata" onchange="updfile('Filedata');" >
				</div>
				<div style="clear:both;"></div>
				<div >&nbsp;图片大小:150 x 150 (px)(格式为 gif, jpg, jpeg, png)</div>
				<input type="hidden" name="dir" value="shops">
				<input type="hidden" name="width" value="150">
				<input type="hidden" name="folder" value="Filedata">
				<input type="hidden" name="sfilename" value="Filedata">
				<input type="hidden" name="fname" value="shopImg">
				<input type="hidden" id="s_Filedata" name="s_Filedata" value="">
				</div>
			</form>
			<!--营业执照图片-->
			<form id="uploadform_Perm" autocomplete="off" style="position:absolute;top:860px;left:341px;z-index:10;" enctype="multipart/form-data" method="POST" target="upload" action="<?php echo U('Home/Shops/uploadPic');?>" >
            <div style="position:relative;">
                <input id="permitImg" name="permitImg" disabled="disabled" class="text"  type="text" value="<?php echo ($object["permitImg"]); ?>" readonly style="margin-right:4px;float:left;margin-left:8px;width:237px;"/>
                <div class="div1">
                    <div class="div2">浏览</div>
                    <input type="file" class="inputstyle" id="Perm" name="Perm" onchange="updfile('Perm');" >
                </div>
                <div style="clear:both;"></div>
              	<div >&nbsp;图片大小:150 x 150 (px)(格式为 gif, jpg, jpeg, png)</div>
                <input type="hidden" name="dir" value="shops">
                <input type="hidden" name="width" value="150">
                <input type="hidden" name="folder" value="Perm">
                <input type="hidden" name="sfilename" value="Perm">
                <input type="hidden" name="fname" value="permitImg">
                <input type="hidden" class="s_Filedata" name="s_Filedata" value="">
            </div>
        	</form>
        	<!--身份证正面图片-->
        	<form id="uploadform_Identi" autocomplete="off" style="position:absolute;top:1080px;left:341px;z-index:10;" enctype="multipart/form-data" method="POST" target="upload" action="<?php echo U('Home/Shops/uploadPic');?>" >
            <div style="position:relative;">
                <input id="identityImg" disabled="disabled" name="identityImg" class="text" type="text" value="<?php echo ($object["identityImg"]); ?>" readonly style="margin-right:4px;float:left;margin-left:8px;width:237px;"/>
                <div class="div1">
                    <div class="div2">浏览</div>
                    <input type="file" class="inputstyle" id="Identi" name="Identi" onchange="updfile('Identi');" >
                </div>
                <div style="clear:both;"></div>
               	<div >&nbsp;图片大小:150 x 150 (px)(格式为 gif, jpg, jpeg, png)</div>
                <input type="hidden" name="dir" value="shops">
                <input type="hidden" name="width" value="150">
                <input type="hidden" name="folder" value="Identi">
                <input type="hidden" name="sfilename" value="Identi">
                <input type="hidden" name="fname" value="identityImg">
                <input type="hidden" class="s_Filedata" name="s_Filedata" value="">
            </div>
        	</form>
        	<!--身份证反面图片-->
        	<form id="uploadform_Identity" autocomplete="off" style="position:absolute;top:1300px;left:341px;z-index:10;" enctype="multipart/form-data" method="POST" target="upload" action="<?php echo U('Home/Shops/uploadPic');?>" >
            <div style="position:relative;">
                <input id="identityRevImg" disabled="disabled" name="identityRevImg" class="text" type="text" value="<?php echo ($object["identityRevImg"]); ?>" readonly style="margin-right:4px;float:left;margin-left:8px;width:237px;"/>
                <div class="div1">
                    <div class="div2">浏览</div>
                    <input type="file" class="inputstyle" id="Identity" name="Identity" onchange="updfile('Identity');" >
                </div>
                <div style="clear:both;"></div>
             	<div >&nbsp;图片大小:150 x 150 (px)(格式为 gif, jpg, jpeg, png)</div>
                <input type="hidden" name="dir" value="shops">
                <input type="hidden" name="width" value="150">
                <input type="hidden" name="folder" value="Identity">
                <input type="hidden" name="sfilename" value="Identity">
                <input type="hidden" name="fname" value="identityRevImg">
                <input type="hidden" class="s_Filedata" name="s_Filedata" value="">
            </div>
        	</form>

	        <form name="myform" method="post" id="myform" autocomplete="off">
	            <div class="form">
	                <div class="item" id="select-regName">
	                    <span class="label"><b class="ftx04">*</b>登录账户：</span>	
	                    <div class="fl item-ifo">
	                        <div class="o-intelligent-regName" style="position:relative;">	    
	                            <input id="loginName" name="loginName" class="text" tabindex="1" maxlength="30" type="text" onkeypress="return WST.isNumberCharKey(event)"/>
	                            <div id="loginNameTip" style="display: inline-block; "></div>
	                        </div>	                        
	                    </div>
	                </div>                
	                <div id="o-password">
	                    <div class="item">
	                        <span class="label"><b class="ftx04">*</b>请设置密码：</span>	
	                        <div class="fl item-ifo">
	                            <input id="loginPwd" name="loginPwd" class="text" tabindex="2" style="ime-mode:disabled;" autocomplete="off" type="password"/>
	                            <label id="pwd_succeed" class="pwdblank"></label>	                           
	                        </div>
	                    </div>	
	                    <div class="item">
	                        <span class="label"><b class="ftx04">*</b>请确认密码：</span>	
	                        <div class="fl item-ifo">
	                            <input id="reUserPwd" name="reUserPwd" class="text" tabindex="3" autocomplete="off" type="password"/>	                            
	                            <label id="pwdRepeat_succeed" class="pwdblank"></label>	                           
	                        </div>
	                    </div>
	                    <div class="item">
	                        <span class="label"><b class="ftx04">*</b>店铺名称：</span>	
	                        <div class="fl item-ifo">
	                            <input id="shopName" name="shopName" class="text" tabindex="3" type='text'/>	                            
	                            <label id="shopName_succeed" class="pwdblank"></label>	                           
	                        </div>
	                    </div>
	                    <div class="item">
	                        <span class="label"><b class="ftx04">*</b>店主姓名：</span>	
	                        <div class="fl item-ifo">
	                            <input id="userName" name="userName" class="text" tabindex="3" type='text'/>	                            
	                            <label id="userName_succeed" class="pwdblank"></label>	                           
	                        </div>
	                    </div>	                    
	                    <div class="item">
	                        <span class="label"><b class="ftx04">*</b>公司名称：</span>	
	                        <div class="fl item-ifo">
	                            <input id="shopCompany" name="shopCompany" class="text" tabindex="3" autocomplete="off" style="" type="text"/>	                           
	                            <label id="shopCompany_succeed" ></label>	                           
	                        </div>
	                    </div>
	                    <div class="item">
	                        <span class="label"><b class="ftx04">*</b>店主手机：</span>	
	                        <div class="fl item-ifo">
	                            <input id="userPhone" name="userPhone" class="text" tabindex="3" autocomplete="off" type="text" maxlength="11"/>	                           
	                            <label id="userPhone_succeed" ></label>	                           
	                        </div>
	                    </div>
	                    <div class="item">
	                        <span class="label">电子邮箱：</span>	
	                        <div class="fl item-ifo">
	                            <input id="userEmail" name="userEmail" class="text" tabindex="3" autocomplete="off" type="text"/>	                                                 
	                        </div>
	                    </div>
	                    <div class="item">
	                        <span class="label"><b class="ftx04">*</b>店铺电话：</span>	
	                        <div class="fl item-ifo">
	                            <input id="shopTel" name="shopTel" class="text" tabindex="3" autocomplete="off" style="" type="text"/>	                           	                           
	                        </div>
	                    </div>
	                    <div class="item">
	                        <span class="label">店铺QQ：</span>	
	                        <div class="fl item-ifo">
	                            <input id="qqNo" name="qqNo" class="text" tabindex="3" autocomplete="off" style="" type="text"/>	                           	                           
	                        </div>
	                    </div>
	                    <div class="item" style='height:auto;position:relative;'>
	                        <span class="label">营业执照编号：</span>	
	                        <div class="fl item-ifo">
	                            <input id="permitNo" name="permitNo" class="text" tabindex="3" autocomplete="off" type="text"/>	                                                 
	                        </div>
	                    </div>
	                    <div class="item" style='height:auto;position:relative;'>
	                        <span class="label"><b class="ftx04">*</b>身份证号码：</span>	
	                        <div class="fl item-ifo">
	                            <input id="idNo" name="idNo" class="text" tabindex="3" autocomplete="off" type="text"/>	                                                 
	                        </div>
	                    </div>
	                    <div class="item" style='height:auto;position:relative;height:210px;'>
	                        <span class="label">店铺图标：</span>	
	                        <div class="fl item-ifo">
	                            <div id="preview_Filedata" style="margin-top: 60px;">
                                <img id='preview' src='' ref='' width='150' style='display:none'/>	 
                                </div>                          
	                        </div>
	                    </div>
	                    <div class="item" style='height:auto;position:relative;height:210px;'>
	                        <span class="label">营业执照图片：</span>	
	                        <div class="fl item-ifo">
	                            <div id="preview_Perm" style="margin-top: 60px;">
                                <img id='permit' src='' ref='' width='150' style='display:none'/>	 
                                </div>                          
	                        </div>
	                    </div>
	                    <div class="item" style='height:auto;position:relative;height:210px;'>
	                        <span class="label"><b class="ftx04">*</b>身份证正面图片：</span>	
	                        <div class="fl item-ifo">
	                            <div id="preview_Identi" style="margin-top: 60px;">
                                <img id='identity' src='' ref='' width='150' style='display:none'/>	 
                                </div>                          
	                        </div>
	                    </div>
	                    <div class="item" style='height:auto;position:relative;height:210px;'>
	                        <span class="label"><b class="ftx04">*</b>身份证反面图片：</span>	
	                        <div class="fl item-ifo">
	                            <div id="preview_Identity" style="margin-top: 60px;">
                                <img id='identityRev' src='' ref='' width='150' style='display:none'/>	 
                                </div>                          
	                        </div>
	                    </div>
	                    <div class="item">
	                        <span class="label"><b class="ftx04">*</b>所属地区：</span>	
	                        <div class="fl item-ifo yj-select">
	                            <select id='areaId1' onchange='javascript:getAreaListForOpen("areaId2",this.value,-1)'>
					               <option value=''>请选择</option>
					               <?php if(is_array($areaList)): $i = 0; $__LIST__ = $areaList;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?><option value='<?php echo ($vo['areaId']); ?>' <?php if($area['parentId'] == $vo['areaId'] ): ?>selected<?php endif; ?>><?php echo ($vo['areaName']); ?></option><?php endforeach; endif; else: echo "" ;endif; ?>
					             </select>
					             <select id='areaId2' onchange='javascript:getAreaListForOpen("areaId3",this.value,0);'>
					               <option value=''>请选择</option>
					             </select>
					             <select id='areaId3'>
					               <option value=''>请选择</option>
					             </select>
	                            <label id="shopTel_succeed" ></label>	                           
	                        </div>
	                    </div>
	                    <div class="item">
	                        <span class="label"><b class="ftx04">*</b>店铺地址：</span>	
	                        <div class="fl item-ifo">
	                            <input id="shopAddress" name="shopAddress" class="text" tabindex="3" autocomplete="off" style="" type="text"/>	                           
	                            <label id="shopAddress_succeed" ></label>	                           
	                        </div>
	                    </div>
	                    <div class="item" style='height:440px;'>
	                    <span class="label">&nbsp;</span>	
                        <div class="fl item-ifo">
                             <div id="mapContainer" style='height:400px;width:660px;'>等待地图初始化...</div>
                             <!--<div style='color:red'>(注意：提交开店申请之后店铺地址将无法修改)</div>-->
				             <div style='display:none'>
				             <input type='text' id='latitude' name='latitude' value=""/>
				             <input type='text' id='longitude' name='longitude' value=""/>
				             <input type='text' id='mapLevel' name='mapLevel' value=""/>
				             </div>
				        </div>
				        </div>
						<div class="item">
	                        <span class="label"><b class="ftx04">*</b>行业：</span>	
	                        <div class="fl item-ifo yj-select">
	                           	<select id='goodsCatId1' class='wstipt' onchange='javascript:getCatListForEdit("goodsCatId2",this.value,0)'>
									<option value=''>请选择</option>
									<?php if(is_array($goodsCatsList)): $i = 0; $__LIST__ = $goodsCatsList;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?><option value='<?php echo ($vo['catId']); ?>' <?php if($object['goodsCatId1'] == $vo['catId'] ): ?>selected<?php endif; ?>><?php echo ($vo['catName']); ?></option><?php endforeach; endif; else: echo "" ;endif; ?>
								</select>
								<select id='goodsCatId2' class='wstipt' >
									<option value=''>请选择</option>
								</select>
	                            <label id="shopTel_succeed" ></label>	                           
	                        </div>
	                    </div>
	                    <div class="item">
	                        <span class="label"><b class="ftx04">*</b>营业状态：</span>	
	                        <div class="fl item-ifo">
	                            <label>
					             <input type="radio" id="shopAtive1" name="shopAtive" style="float: none" value="1">营业中&nbsp;&nbsp;
					            </label>
					            <label>
					             <input type="radio" id="shopAtive0" name="shopAtive" style="float: none" value="0" checked="">休息中
					            </label>	                           
	                        </div>
	                    </div>
	                    <div class="item" style='height:auto;'>
	                        <span class="label"><b class="ftx04">*</b>配送区域：</span>	
	                        <div class="fl item-ifo yj-select" style='width:670px;'>
             					 <div id='areaTree'></div>	                           
	                        </div>
	                    </div>
	                    <div class="item">
	                        <span class="label"><b class="ftx04">*</b>订单配送起步价：</span>	
	                        <div class="fl item-ifo">
	                            <input type="text" id="deliveryStartMoney" class="text" value="0.0" onkeypress="return WST.isNumberdoteKey(event)" onkeyup="javascript:WST.isChinese(this,1)" maxlength="8">            
	                        </div>
	                    </div>
	                    <div class="item">
	                        <span class="label"><b class="ftx04">*</b>包邮起步价：</span>	
	                        <div class="fl item-ifo">
	                            <input type="text" id="deliveryFreeMoney" class='text' value='0.0' onkeypress="return WST.isNumberdoteKey(event)" onkeyup="javascript:WST.isChinese(this,1)" maxlength="8">	                           
	                        </div>
	                    </div>
	                    <div class="item">
	                        <span class="label"><b class="ftx04">*</b>邮费：</span>	
	                        <div class="fl item-ifo">
	                           <input type="text" id="deliveryMoney" class='text' value='0.0' onkeypress="return WST.isNumberdoteKey(event)" onkeyup="javascript:WST.isChinese(this,1)" maxlength="8">
	                        </div>
	                    </div>
	                    <div class="item">
	                        <span class="label"><b class="ftx04">*</b>平均消费金额：</span>	
	                        <div class="fl item-ifo">
	                           <input type="text" id="avgeCostMoney" class='text' value='0.0' onkeypress="return WST.isNumberdoteKey(event)" onkeyup="javascript:WST.isChinese(this,1)" maxlength="8">
	                        </div>
	                    </div>
	                    <div class="item">
	                        <span class="label"><b class="ftx04">*</b>能否开发票：</span>	
	                        <div class="fl item-ifo">
	                           <label>
					             <input type="radio" id="isInvoice1" style="float: none" onclick="javascript:isInvoce(true)" name="isInvoice" value="1">能&nbsp;&nbsp;
					             </label>
					             <label>
					             <input type="radio" id="isInvoice0" style="float: none" onclick="javascript:isInvoce(false)" name="isInvoice" value="0" checked>否
					             </label>
	                        </div>
	                    </div>
	                   <div id='invoiceRemarkstr' class="item" style='display:none'>
			             <span class="label"><b class="ftx04">*</b>发票说明：</span>
			             <div class="fl item-ifo">
			             <input type='text' id='invoiceRemarks' class="text" value='<?php echo ($object["invoiceRemarks"]); ?>' style='width:350px;' maxLength='100'/>
			             </div>
			            </div>    
	                    <div class="item">
	                        <span class="label"><b class="ftx04">*</b>转账银行：</span>	
	                        <div class="fl item-ifo yj-select">
	                           <select id='bankId'>
					                <option value=''>请选择</option>
					                <?php if(is_array($bankList)): $i = 0; $__LIST__ = $bankList;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?><option value='<?php echo ($vo['bankId']); ?>'><?php echo ($vo['bankName']); ?></option><?php endforeach; endif; else: echo "" ;endif; ?>
					             </select>
	                        </div>
	                    </div> 
	                    <div class="item">
	                        <span class="label"><b class="ftx04">*</b>银行卡卡号：</span>	
	                        <div class="fl item-ifo">
	                           <input type="text" class='text' id="bankNo" maxlength="25" size="50">
	                        </div>
	                    </div>
	                    <div class="item">
	                        <span class="label"><b class="ftx04">*</b>营业时间：</span>	
	                        <div class="fl item-ifo yj-select">
	                           <select id='serviceStartTime'></select>
                               <div class='tleft'>&nbsp;至&nbsp;</div> 
                               <select id='serviceEndTime'></select>
	                        </div>
	                    </div>    
	                  	<div class="item" id="authcodeDiv">
	                            <span class="label"><b class="ftx04">*</b>验证码：</span>	
	                            <div class="fl item-ifo">
	                                <input id="authcode" style="ime-mode:disabled" name="authcode" class="text text-1" tabindex="6" autocomplete="off" maxlength="6" type="text"/>
	                                <label class="img">
	                                    <img style='vertical-align:middle;cursor:pointer;height:39px;' class='verifyImg' src='' title='刷新验证码' onclick='javascript:getVerify()'/> 
									 </label>      	
	                                <label class="ftx23">&nbsp;看不清？<a href="javascript:getVerify()" onclick="setTimeout(verc,50);" class="flk13">换一张</a></label>
	                            </div>
	                 	</div>	
	                 	 
	                </div>
	                <input name="qVoCgWxtvu" value="EqaRK" type="hidden"/>	
	                <div class="item item-new">
	                    <span class="label">&nbsp;</span>	
	                    <div class="fl item-ifo">
	                        <label>
	                        <input class="checkbox" id="protocol" name="protocol" type="checkbox"/>
	                                                                        我已阅读并同意</label><a href="javascript:;" class="blue" id="protocolInfo" onclick="showXiey();">《用户注册协议》</a>                       
	                        <label id="protocol_error" class="error hide">请接受服务条款</label>
	                    </div>
	                </div>
	                <div class="item">
	                    <span class="label">&nbsp;</span>
	                    <input class="btn-img btn-regist" id="registsubmit" value="立即注册" tabindex="8"  type="submit"/>
	                </div>
	            </div>
	            <div class="phone">
	                
	            </div>
	      		<span class="clr"></span>
	        </form>
	    </div>
	</div>
	
</div>


<div class="wst-footer-hp-box">
	<div class="wst-footer">
		<div class="wst-footer-hp-ck1">
			<?php if(is_array($helps)): $k1 = 0; $__LIST__ = $helps;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo1): $mod = ($k1 % 2 );++$k1;?><div class="wst-footer-wz-ca">
				<div class="wst-footer-wz-pt">
				   <!--  <img src="/Tpl/default/images/a<?php echo ($k1); ?>.jpg" height="18" width="18"/> -->
					<span class="wst-footer-wz-pn"><?php echo ($vo1["catName"]); ?></span>
					<div style='margin-left:30px;'>
						<?php if(is_array($vo1['articlecats'])): $k2 = 0; $__LIST__ = $vo1['articlecats'];if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo2): $mod = ($k2 % 2 );++$k2;?><a href="<?php echo U('Home/Articles/index/',array('articleId'=>$vo2['articleId']));?>"><?php echo ($vo2['articleTitle']); ?></a><br/><?php endforeach; endif; else: echo "" ;endif; ?>
					</div>
				</div>
			</div><?php endforeach; endif; else: echo "" ;endif; ?>
			
			<div class="wst-footer-wz-clt">
				<div style="padding-top:5px;line-height:25px;">
				   <!--  <img src="/Tpl/default/images/a6.jpg" height="18" width="18"/> -->
					<span class="wst-footer-wz-kf">联系客服</span>
					<div style='margin-left:30px;'>
						<a href="#">联系电话</a><br/>
						<?php if($CONF['phoneNo'] != ''): ?><span class="wst-footer-pno"><?php echo ($CONF['phoneNo']); ?></span><br/><?php endif; ?>
						<?php if($CONF['qqNo'] != ''): ?><a  style="text-decoration: none;" href="tencent://message/?uin=<?php echo ($CONF['qqNo']); ?>&Site=QQ交谈&Menu=yes">
						<img border="0" src="/Tpl/default/images/qq.gif" alt="QQ交谈" width="121" height="40" />
						</a><br/><?php endif; ?>
						
					</div>
				</div>
			</div>
			<div class="wst-clear"></div>
		</div>
	    
	   <div class="wst-footer-fl-box">
	<div class="wst-footer" >
		<div class="wst-footer-cld-box">
			 <div class="wst-footer-fl">友情链接：</div>
			<div style="padding-left:30px;">
				<?php if(is_array($friendLikds)): $k = 0; $__LIST__ = $friendLikds;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($k % 2 );++$k;?><div style="float:left;"><a style="font-size: 14px;" href="<?php echo ($vo["friendlinkUrl"]); ?>" target="_blank"><?php echo ($vo["friendlinkName"]); ?></a>&nbsp;&nbsp;</div><?php endforeach; endif; else: echo "" ;endif; ?>
				<div class="wst-clear"></div>
			</div>
		</div>
	</div>
</div>
	<!-- 	<div class="wst-footer-hp-ck2">
			<img src="/Tpl/default/images/alipay.jpg" height="40" width="120"/>支付宝签约商家&nbsp;|&nbsp;
			<span class="wst-footer-frd">正品保障</span><span >100%原产地</span>&nbsp;|&nbsp;
			<span class="wst-footer-frd">7天退货保障</span>购物无忧&nbsp;|&nbsp;
			<span class="wst-footer-frd">免费配送</span>满98包邮&nbsp;|&nbsp;
			<span class="wst-footer-frd">货到付款</span>400城市送货上门
		</div> -->
	    
	</div>
</div>
		<div class="wst-footer-hp-ck3">
	        <div class="links" style="background: #666666;text-align: center;padding-top: 10px;color: #ffffff;"> 
	            <?php $_result=WSTNavigation(1);if(is_array($_result)): $i = 0; $__LIST__ = $_result;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?><a rel="nofollow" <?php if($vo['isOpen'] == 1): ?>target="_blank"<?php endif; ?> href="<?php echo ($vo['navUrl']); ?>"><?php echo ($vo['navTitle']); ?></a>&nbsp;<?php if($vo['end'] == 0): ?>|&nbsp;<?php endif; endforeach; endif; else: echo "" ;endif; ?>
	        </div>
	        
	        <div style="background: #666666;" class="copyright">
	         
	         <?php if($CONF['mallFooter']!=''){ echo htmlspecialchars_decode($CONF['mallFooter']); } ?>
	      	<?php if($CONF['visitStatistics']!=''){ echo htmlspecialchars_decode($CONF['visitStatistics'])."<br/>"; } ?>
	        <?php if($CONF['mallLicense'] ==''): ?><div>
				Copyright©2015 Powered By <a target="_blank" href="###">o2omall</a>
			</div>
			<?php else: ?>
				<div id="wst-mallLicense" data='1' style="display:none;">Copyright©2015 Powered By <a target="_blank" href="###">o2omall</a></div><?php endif; ?>
	        </div>
	    </div>

<link rel="stylesheet" type="text/css" href="/Tpl/default/css/cart.css" />
<script src="/Tpl/default/js/login.js"></script>
<script type="text/javascript" src="/Tpl/default/js/cart/common.js?v=725"></script>
<script type="text/javascript" src="/Tpl/default/js/cart/quick_links.js"></script>
<!--[if lte IE 8]>
<script src="/Tpl/default/js/cart/ieBetter.js"></script>
<![endif]-->
<script src="/Tpl/default/js/cart/parabola.js"></script>
<!--右侧贴边导航quick_links.js控制-->
	<div id="flyItem" class="fly_item" style="display:none;">
		<p class="fly_imgbox">
		<img src="/Tpl/default/images/item-pic.jpg"
			width="30" height="30">
		</p>
	</div>
	<div class="mui-mbar-tabs">
		<div class="quick_link_mian">
			<div class="quick_links_panel">
				<div id="quick_links" class="quick_links">
				
					<li id="shopCart"><a href="#" class="message_list"><i class="message"></i>
					<div class="span"></div> <span class="cart_num">0</span></a></li>
						<li><a href="#" class="my_qlinks" style="margin-top: 5px;background-image: url('/Tpl/default/images/r_kefu.png');height: 41px;"><i class="setting"></i></a>
						<div class="ibar_login_box status_login">
							<?php if($WST_USER['userId'] > 0): ?><div class="avatar_box">
								<p class="avatar_imgbox">
									<?php if($WST_USER['userPhoto'] != ''): ?><img src="/<?php echo ($WST_USER['userPhoto']); ?>"  style="width:100px;height: 100px;"/>
									<?php else: ?>
										<img src="/Tpl/default/images/no-img_mid_.jpg"  /><?php endif; ?>
								</p>
								<?php if($WST_USER['userId'] > 0): ?><ul class="user_info">
									<li>用户名：<?php echo ($WST_USER['loginName']); ?></li>
									<li>级&nbsp;别：普通会员</li>
								</ul><?php endif; ?>
							</div>
							
							<div class="ibar_recharge-btn">
								<input type="button" value="我的订单" onclick="getMyOrders();"/>
							</div>
							<i class="icon_arrow_white"></i>
						</div> <?php else: ?>
							<div style="margin: 0 auto;padding: 15px 0; width: 220px;">
							<div class="ibar_recharge-field">
								<label>帐号</label>
								<div class="ibar_recharge-fl">
									<div class="ibar_recharge-iwrapper">
										<input id="loginName" name="loginName" value="<?php echo ($loginName); ?>" type="text" name="19" placeholder="用户名/手机号/邮箱" />
									</div>
									<i class="ibar_username-contact"></i>
								</div>
							</div>
							<div class="ibar_recharge-field">
								<label>密码</label>
								<div class="ibar_recharge-fl">
									<div class="ibar_recharge-iwrapper">
										<input id="loginPwd" name="loginPwd" type="password" name="19" placeholder="密码" />
									</div>
									<i class="ibar_password-contact"></i>
								</div>
							</div>
							<div class="ibar_recharge-field">
								<label>验证码</label>
								<div class="ibar_recharge-fl" style="width:80px;">
									<div class="ibar_recharge-iwrapper">
										<input id="verify" style="ime-mode:disabled;width:80px;" name="verify" class="text text-1" tabindex="6" autocomplete="off" maxlength="6" type="text" placeholder="验证码"/>
									</div>
								</div>
								<label class="img" onclick="javascript:getVerify()">
				                	<img style='vertical-align:middle;cursor:pointer;height:30px;width:93px;' class='verifyImg' src='/Tpl/default/images/clickForVerify.png' title='刷新验证码' onclick='javascript:getVerify()'/> 
								</label>
							</div>
							<div class="ibar_recharge-btn">
								<input type="button" value="登录" onclick="checkLoginInfo();"/>
							</div>
							</div><?php endif; ?></li>

						<?php if($CONF['qqNo'] != ''): ?><li>
								<a href="tencent://message/?uin=<?php echo ($CONF['qqNo']); ?>&Site=QQ交谈&Menu=yes" style="/*padding-top:5px;*/padding-bottom:5px;margin-bottom: 5px;">
								<img src="/Tpl/default/images/r_qq.png"  height="39" width="40" />
								</a>
							</li><?php endif; ?>
				</div>
				<div class="quick_toggle">

					<li><a href="#top" class="return_top" style="height: 60px;"><i class="top"></i></a></li>
				</div>
			</div>
			<div id="quick_links_pop" class="quick_links_pop hide"></div>
		</div>
	</div>
	<script type="text/javascript">
	var numberItem = <?php echo WSTCartNum();?>;
	$('.cart_num').html(numberItem);
	$(".quick_links_panel li").mouseenter(function() {
		$(this).children(".mp_tooltip").animate({
			left : -92,
			queue : true
		});
		$(this).children(".mp_tooltip").css("visibility", "visible");
		$(this).children(".ibar_login_box").css("display", "block");
	});
	$(".quick_links_panel li").mouseleave(function() {
		$(this).children(".mp_tooltip").css("visibility", "hidden");
		$(this).children(".mp_tooltip").animate({
			left : -121,
			queue : true
		});
		$(this).children(".ibar_login_box").css("display", "none");
	});
	$(".quick_toggle li").mouseover(function() {
		$(this).children(".mp_qrcode").show();
	});
	$(".quick_toggle li").mouseleave(function() {
		$(this).children(".mp_qrcode").hide();
	});

	// 元素以及其他一些变量
	var eleFlyElement = document.querySelector("#flyItem"), eleShopCart = document
			.querySelector("#shopCart");
	eleFlyElement.style.visibility = "hidden";
	
	var numberItem = 0;
	// 抛物线运动
	var myParabola = funParabola(eleFlyElement, eleShopCart, {
		speed : 200, //抛物线速度
		curvature : 0.0012, //控制抛物线弧度
		complete : function() {
			eleFlyElement.style.visibility = "hidden";
			jQuery.post(domainURL +"/index.php/Home/Cart/getCartGoodCnt/" ,{"axm":1},function(data) {
				var cart = WST.toJson(data);
				eleShopCart.querySelector("span").innerHTML = cart.goodscnt;
			});
			
		}
	});
	// 绑定点击事件
	if (eleFlyElement && eleShopCart) {
		[].slice
				.call(document.getElementsByClassName("btnCart"))
				.forEach(
						function(button) {
							button
									.addEventListener(
											"click",
											function(event) {
												// 滚动大小
												var scrollLeft = document.documentElement.scrollLeft
														|| document.body.scrollLeft
														|| 0, scrollTop = document.documentElement.scrollTop
														|| document.body.scrollTop
														|| 0;
												eleFlyElement.style.left = event.clientX
														+ scrollLeft + "px";
												eleFlyElement.style.top = event.clientY
														+ scrollTop + "px";
												eleFlyElement.style.visibility = "visible";
												$(eleFlyElement).show();
												// 需要重定位
												myParabola.position().move();
											});
						});
	}

	function getMyOrders(){
		document.location.href = "<?php echo U('Home/Orders/queryByPage/');?>";
	}
</script>
<script src="/Public/js/common.js"></script>
<script src="/Tpl/default/js/common.js"></script>
<script src="/Tpl/default/js/head.js" type="text/javascript"></script>

<script src="/Public/plugins/layer/layer.min.js"></script>
<script src="/Public/plugins/formValidator/formValidator-4.1.3.js"></script>
<script src="/Tpl/default/js/open_shop.js"></script>
<script type="text/javascript" src="http://webapi.amap.com/maps?v=1.3&key=2b53397482f52f92dcb7c528d99d9fca"></script>
<script type="text/javascript">
   $(function(){
	   getAreaListForOpen("areaId2",'<?php echo ($area["parentId"]); ?>',0,'<?php echo ($area["areaId"]); ?>');
	   getAreaListForOpen("areaId3",'<?php echo ($area["areaId"]); ?>',1);
   })
</script>
<style>
	.layui-layer-btn a{background:#e23c3d;}
	</style>

</body>
</html>