<?php if (!defined('THINK_PATH')) exit();?><!doctype html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
      <title>卖家中心 - <?php echo ($CONF['mallTitle']); ?></title>
      <meta name="keywords" content="<?php echo ($CONF['mallKeywords']); ?>" />
        <meta name="description" content="<?php echo ($CONF['mallDesc']); ?>,卖家中心" />
      <meta http-equiv="Expires" content="0">
    <meta http-equiv="Pragma" content="no-cache">
    <meta http-equiv="Cache-control" content="no-cache">
    <meta http-equiv="Cache" content="no-cache">
      <link rel="stylesheet" href="/Tpl/default/css/common.css" />
      <link rel="stylesheet" href="/Tpl/default/css/shop.css">
      <link rel="stylesheet" type="text/css" href="/Public/plugins/webuploader/webuploader.css" />
      <style type="text/css">

      .wst-nav-box  .login-out{
          width: auto;
          background: none;
          display: inline-block;
          text-align: center;
          float: right;
        }
        .shops{
          color:#FFFFFF;
          display: inline-block;
          float: left;
        }
      </style>
        <script src="/Public/js/jquery.min.js"></script>
        <script src="/Public/plugins/lazyload/jquery.lazyload.min.js?v=1.9.1"></script>
        <script>

          var publicurl = '/Public/';
          var shopId = '<?php echo ($orderInfo["order"]["shopId"]); ?>';
          var pageCount='<?php echo ($receiveOrders["totalPage"]); ?>';
          var current='<?php echo ($receiveOrders["currPage"]); ?>';
          var domainURL = "<?php echo WSTDomain();?>";
          var publicurl = "/Public";
          var currCityId = "<?php echo ($currArea['areaId']); ?>";
          var currCityName = "<?php echo ($currArea['areaName']); ?>";
          var currDefaultImg = "<?php echo WSTDomain();?>/<?php echo ($CONF['goodsImg']); ?>";
          var wstMallName = "<?php echo ($CONF['mallName']); ?>";
          //   $(function() {
          //   $('.lazyImg').lazyload({ effect: "fadeIn",failurelimit : 10,threshold: 200,placeholder:currDefaultImg});
          // });
          var publicurl = '/Public/';
          var shopId = '<?php echo ($orderInfo["order"]["shopId"]); ?>';
          var pageCount='<?php echo ($receiveOrders["totalPage"]); ?>';
          var current='<?php echo ($receiveOrders["currPage"]); ?>';
          var ThinkPHP = window.Think = {
              "ROOT"   : "",
              "APP"    : "",
              "PUBLIC" : "/Public",
              "DEEP"   : "<?php echo C('URL_PATHINFO_DEPR');?>",
              "MODEL"  : ["<?php echo C('URL_MODEL');?>", "<?php echo C('URL_CASE_INSENSITIVE');?>", "<?php echo C('URL_HTML_SUFFIX');?>"],
              "VAR"    : ["<?php echo C('VAR_MODULE');?>", "<?php echo C('VAR_CONTROLLER');?>", "<?php echo C('VAR_ACTION');?>"]
          }
        </script>
        <script src="/Public/js/think.js"></script>
    <?php echo WSTLoginTarget(1);?>
    </head>
    <body>
        <div class="wst-wrap">
          <div class='wst-header'>
      <div class='wst-user-top' style="display: none;">
      <div class="wst-user-top-main">
          <div class='wst-user-top-logo'>
            <a href="<?php echo WSTDomain();?>"  title='商城首页'>
            <img src="<?php echo WSTDomain();?>/<?php echo ($CONF['mallLogo']); ?>" height="132" width='240'/>
            </a>
          </div>
          <div class='wst-user-top-search'>
            <div class="search-box">
              <input id="wst-search-type" type="hidden" value="1"/>
              <input id="keyword" class="wst-search-ipt" me="q" tabindex="9" placeholder="搜索 商品" autocomplete="off" >
              <div id="btnsch" class="wst-search-btn">搜&nbsp;索</div>
            </div>
          </div>

        </div>
      </div>
      <div class="wst-shop-nav">
        <div class="wst-nav-box">
          <li class="liselect shops"><a href="<?php echo U('Home/Shops/index');?>" >我是卖家</a></li>
          <li class="fore1 login-out" id="loginbar" >


          <span>
            <?php if(!$WST_USER['userId']): ?><a href="<?php echo U('Home/Users/login');?>">[登录]</a>
            <a href="<?php echo U('Home/Users/regist');?>" class="link-regist">[免费注册]</a><?php endif; ?>
            <?php if($WST_USER['userId'] > 0): ?><a href="javascript:logout();">[退出]</a><?php endif; ?>
          </span>
          </li>
          <div class="wst-clear"></div>
        </div>
      </div>

      <div class="wst-clear;"></div>
    </div>
          <div class='wst-nav'></div>
          <div class='wst-main'>
            <div class='wst-menu'>


        <span class='wst-menu-title'><span></span>订单管理</span>
                <div <?php if($selected_menu == 1): ?>style="display:block;"<?php else: ?>style="display:none;"<?php endif; ?>>
        <a href='<?php echo U("Home/Orders/toShopOrdersList");?>' onclick="createCookie('menu','1');"><li id='li_toShopOrdersList' <?php if($umark == "toShopOrdersList"): ?>class='liselect'<?php endif; ?>>订单管理<span style="display:none;" class="wst-msg-tips-box"></span></li></a>

                </div>
                <!--
                                <a href='<?php echo U("Home/Orders/toShopGroupOrders");?>' onclick="createCookie('menu','1');"><li id='li_toShopGroupOrders' <?php if($umark == "toShopGroupOrders"): ?>class='liselect'<?php endif; ?>>团购活动<span style="display:none;" class="wst-msg-tips-box"></span></li></a>
                <a href='<?php echo U("Home/Orders/toShopSeckillOrders");?>' onclick="createCookie('menu','1');"><li id='li_toShopSeckillOrders' <?php if($umark == "toShopSeckillOrders"): ?>class='liselect'<?php endif; ?>>秒杀活动<span style="display:none;" class="wst-msg-tips-box"></span></li></a>
        <span class='wst-menu-title'><span></span>拍卖管理</span>
                <div <?php if($selected_menu == 2): ?>style="display:block;"<?php else: ?>style="display:none;"<?php endif; ?>>
        <a href="<?php echo U('Home/Goods/queryPassGoodsByPage/');?>" onclick="createCookie('menu','2');"><li <?php if($umark == "queryPassGoodsByPage"): ?>class='liselect'<?php endif; ?>>新增拍卖</li></a>
                <a href='<?php echo U("Home/Orders/toShopAuctionOrders");?>' onclick="createCookie('menu','2');"><li id='li_toShopAuctionOrders' <?php if($umark == "toShopAuctionOrders"): ?>class='liselect'<?php endif; ?>>拍卖活动<span style="display:none;" class="wst-msg-tips-box"></span></li></a>
                <a href='<?php echo U("Home/Orders/toShopAuctionOrdersList");?>' onclick="createCookie('menu','2');"><li id='li_toShopAuctionOrdersList' <?php if($umark == "toShopAuctionOrdersList"): ?>class='liselect'<?php endif; ?>>拍卖订单<span style="display:none;" class="wst-msg-tips-box"></span></li></a>
        </div>
                <span class='wst-menu-title'><span></span>售后管理</span>-->
                <div <?php if($selected_menu == 9): ?>style="display:block;"<?php else: ?>style="display:none;"<?php endif; ?>>
                <!--
                <a href="<?php echo U('Home/Refund/refundList');?>"><li <?php if($umark == "refundList"): ?>class='liselect'<?php endif; ?>>退款信息</li></a>
                <a href="<?php echo U('Home/Complain/index');?>" onclick="createCookie('menu','9');"><li <?php if($umark == "complain"): ?>class='liselect'<?php endif; ?>>投诉信息</li></a>
                
                <a href="<?php echo U('Home/GoodsSun/index');?>"><li <?php if($umark == "goodsSun"): ?>class='liselect'<?php endif; ?>>评价信息</li></a>-->
                </div>


                <span class='wst-menu-title' ><span></span>商品管理</span>
                <div <?php if($selected_menu == 3): ?>style="display:block;"<?php else: ?>style="display:none;"<?php endif; ?>>
        <a href="<?php echo U('Home/ShopsCats/index/');?>" onclick="createCookie('menu','3');"><li <?php if($umark == "index"): ?>class='liselect'<?php endif; ?>>商品分类</li></a>
                <a href="<?php echo U('Home/Goods/queryOnSaleByPage/');?>" onclick="createCookie('menu','3');"><li <?php if($umark == "queryOnSaleByPage"): ?>class='liselect'<?php endif; ?>>出售中的商品</li></a>
                <a href="<?php echo U('Home/Goods/queryPenddingByPage/');?>" onclick="createCookie('menu','3');"><li <?php if($umark == "queryPenddingByPage"): ?>class='liselect'<?php endif; ?>>待审核商品</li></a>
                <a href="<?php echo U('Home/Goods/queryUnSaleByPage/');?>" onclick="createCookie('menu','3');"><li <?php if($umark == "queryUnSaleByPage"): ?>class='liselect'<?php endif; ?>>仓库中的商品</li></a>
                <a href="<?php echo U('Home/Goods/toEdit/',array('umark'=>'toEditGoods'));?>" onclick="createCookie('menu','3');"><li <?php if($umark == "toEditGoods"): ?>class='liselect'<?php endif; ?>>新增商品</li></a>
                <!--<a href="<?php echo U('Home/GoodsAppraises/index/');?>" onclick="createCookie('menu','3');"><li <?php if($umark == "GoodsAppraises"): ?>class='liselect'<?php endif; ?>>评价管理</li></a>
                
                <a href="<?php echo U('Home/Imports/index');?>" onclick="createCookie('menu','3');"><li <?php if($umark == "Imports"): ?>class='liselect'<?php endif; ?>>数据导入</li></a>
                <a href="<?php echo U('Home/Share/shopShare/');?>" onclick="createCookie('menu','3');"><li <?php if($umark == "goodsShare"): ?>class='liselect'<?php endif; ?>>晒单审核</li></a>-->
                <!--<a href="<?php echo U('Home/circle/index/');?>" onclick="createCookie('menu','3');"><li <?php if($umark == "Circle"): ?>class='liselect'<?php endif; ?>>圈子管理</li></a>-->
                <a href="<?php echo U('Home/AttributeCats/index');?>" onclick="createCookie('menu','3');"><li <?php if($umark == "AttributeCats"): ?>class='liselect'<?php endif; ?>>商品类型</li></a>
                
        </div>

        <!--
          <span class="wst-menu-title"><span></span>云购管理</span>
                <div <?php if($selected_menu == 4): ?>style="display:block;"<?php else: ?>style="display:none;"<?php endif; ?>>
        <a href="<?php echo U('Home/Goods/yungoods');?>" onclick="createCookie('menu','4');"><li <?php if(ACTION_NAME == "yungoods" OR ACTION_NAME == "yunedit"): ?>class='liselect'<?php endif; ?>>商品管理</li></a>
                <a href="<?php echo U('Home/Goods/addgoods');?>" onclick="createCookie('menu','4');"><li <?php if(ACTION_NAME == "addgoods"): ?>class='liselect'<?php endif; ?>>新增商品</li></a>
        <a href="<?php echo U('Home/Goods/yunorder');?>" onclick="createCookie('menu','4');"><li <?php if($sng == "def"): ?>class='liselect'<?php endif; ?>>订单列表</li></a>
        <a href="<?php echo U('Home/Goods/yunorder',array('sng' => 'zj'));?>" onclick="createCookie('menu','4');"><li <?php if($sng == "zj"): ?>class='liselect'<?php endif; ?>>中奖订单</li></a>
        <a href="<?php echo U('Home/Goods/yunorder',array('sng' => 'notsend'));?>" onclick="createCookie('menu','4');"><li <?php if($sng == "notsend"): ?>class='liselect'<?php endif; ?>>等待发货订单</li></a>
        <a href="<?php echo U('Home/Goods/yunorder',array('sng' => 'sendok'));?>" onclick="createCookie('menu','4');"><li <?php if($sng == "sendok"): ?>class='liselect'<?php endif; ?>>已发货订单</li></a>
        <a href="<?php echo U('Home/Goods/yunorder',array('sng' => 'ok'));?>" onclick="createCookie('menu','4');"><li <?php if($sng == "ok"): ?>class='liselect'<?php endif; ?>>已完成订单</li></a>
        </div>
        -->

                <span class='wst-menu-title'><span></span>网店设置</span>
                <div <?php if($selected_menu == 7): ?>style="display:block;"<?php else: ?>style="display:none;"<?php endif; ?>>
        <a href="<?php echo U('Home/Apply/shopsIndex');?>" onclick="createCookie('menu','7');"><li <?php if($umark == "shopsIndex"): ?>class='liselect'<?php endif; ?>>提现管理</li></a>
              <a href="<?php echo U('Home/Shops/toEdit/');?>" onclick="createCookie('menu','7');"><li <?php if($umark == "toEdit"): ?>class='liselect'<?php endif; ?>>店铺资料</li></a>
                <!-- <a href="" onclick="createCookie('menu','7');"><li class='liselect'>店铺设置</li></a> -->
                <a href="<?php echo U('Home/Messages/queryByPage/');?>" onclick="createCookie('menu','7');"><li id='li_queryMessageByPage' <?php if($umark == "queryMessageByPage"): ?>class='liselect'<?php endif; ?>>商城消息<span style="display:none;" class="wst-msg-tips-box"></span></li></a>
                <a href="<?php echo U('Home/Shops/toEditPass');?>" onclick="createCookie('menu','7');"><li <?php if($umark == "toEditPass"): ?>class='liselect'<?php endif; ?>>修改密码</li></a>
                <a style="display: none;" href="<?php echo U('Home/Express/index');?>" onclick="createCookie('menu','7');"><li <?php if($umark == "express"): ?>class='liselect'<?php endif; ?>>物流管理</li></a>
        </div>
                <span class='wst-menu-title' ><span></span>优惠活动</span>
                  <div <?php if($selected_menu == 8): ?>style="display:block;"<?php else: ?>style="display:none;"<?php endif; ?>>
          <a href="<?php echo U('Home/Youhui/shopindex');?>" onclick="createCookie('menu','8');"><li <?php if($umark == "youhuilist"): ?>class='liselect'<?php endif; ?>>优惠券列表</li></a>
                  <a href="<?php echo U('Home/Youhui/shoprecord');?>" onclick="createCookie('menu','8');"><li <?php if($umark == "youhuiused"): ?>class='liselect'<?php endif; ?>>已使用优惠券列表</li></a>
                  <!-- <a style="display: none;" href="" onclick="createCookie('menu','8');"><li <?php if($umark == "ggkshoplist"): ?>class='liselect'<?php endif; ?>>刮刮卡列表</li></a>
                  <a style="display: none;" href="" onclick="createCookie('menu','8');"><li <?php if($umark == "ggkchecksnlist"): ?>class='liselect'<?php endif; ?>>刮刮卡中奖码查询</li></a> -->
          </div>


      </div>


            <div class='wst-content'>
            
<div>
	<div class='wst-page-header'>
		卖家中心&nbsp;>&nbsp;帐户概览
	</div>
	<div style="height:158px;">
		<table style="width:800px;margin-top:25px;">
			<tbody>
				<tr>
					<td width="140">
						<div class='wst-shophome-img'>
							<a target="_blank" href="<?php echo U('Home/Shops/toShopHome/',array('shopId'=>$shopInfo['shop']['shopId']));?>">
								<img src="/<?php echo ($shopInfo['shop']['shopImg']); ?>" height="120" width="120" />
							</a>
						</div>
					</td>
					<td style="vertical-align:top;line-height:25px;">
						<div style="font-weight:bolder;"><?php echo ($shopInfo['shop']['shopName']); ?></div>
						<div style="">店铺名称：<a target="_blank" href="<?php echo U('Home/Shops/toShopHome/',array('shopId'=>$shopInfo['shop']['shopId']));?>"><span style="color:#55AAFF"><?php echo ($shopInfo['shop']['shopName']); ?></a></span></div>
						<div style="">店铺状态：
						<?php if($shopInfo['shop']['shopStatus'] == 1): if(($shopInfo['shop']['shopAtive'] == 1) and ($shopInfo['shop']['shopStatus'] == 1)): ?>营业中，
							<?php else: ?>
								休息中，<?php endif; endif; ?>
						<?php if($shopInfo['shop']['shopStatus'] == 1): ?>已审核
						<?php elseif($shopInfo['shop']['shopStatus'] == -2): ?>
							已停止
						<?php elseif($shopInfo['shop']['shopStatus'] == -1): ?>
							已拒绝
						<?php elseif($shopInfo['shop']['shopStatus'] == 0): ?>
							待审核<?php endif; ?>
						</div>
					</td>
					<td width="280" style="vertical-align:top;line-height:25px;">
						<div style="font-weight:bolder;">店铺动态评分</div>
						<div style="">商品描述:
							<?php $__FOR_START_17922__=0;$__FOR_END_17922__=$shopInfo['details']['goodsScore'];for($i=$__FOR_START_17922__;$i < $__FOR_END_17922__;$i+=1){ ?><img src="/Tpl/default/images/icon_score_yes.png"/><?php } ?>
							<?php echo ($shopInfo['details']['goodsScore']); ?>分
						</div>
						<div style="">时效评分:
							<?php $__FOR_START_6931__=0;$__FOR_END_6931__=$shopInfo['details']['timeScore'];for($i=$__FOR_START_6931__;$i < $__FOR_END_6931__;$i+=1){ ?><img src="/Tpl/default/images/icon_score_yes.png"/><?php } ?>
							<?php echo ($shopInfo['details']['timeScore']); ?>分
						</div>
						<div style="">服务评分:
							<?php $__FOR_START_336__=0;$__FOR_END_336__=$shopInfo['details']['serviceScore'];for($i=$__FOR_START_336__;$i < $__FOR_END_336__;$i+=1){ ?><img src="/Tpl/default/images/icon_score_yes.png"/><?php } ?>
							<?php echo ($shopInfo['details']['serviceScore']); ?>分
						</div>
					</td>
				</tr>
			</tbody>
		</table>
	</div>
	
	<div class='wst-shophome-area'>
		<div class='wst-shophome-nav'>
			<div class='header'>
				店铺提示
			</div>
			<div class='main'>
				<div style="font-weight:bolder;">您需要关注的店铺情况：</div>
				<div style="color:#55AAFF;">
					商品提示：
					<span>待审核商品（<span style="color:red;"><?php echo (intval($shopInfo['details']['waitGoodsCnt'])); ?></span>）</span>&nbsp;&nbsp;&nbsp;&nbsp;
					<span>仓库中商品（<span style="color:red;"><?php echo (intval($shopInfo['details']['waitSaleGoodsCnt'])); ?></span>）</span>&nbsp;&nbsp;&nbsp;&nbsp;
					<span>买家评价（<span style="color:red;"><?php echo (intval($shopInfo['details']['appraisesCnt'])); ?></span>）</span>
				</div>
			</div>
			<div class='current' >出售中的商品（<?php echo (intval($shopInfo['details']['onSaleGoodsCnt'])); ?>）&nbsp;&nbsp;&nbsp;&nbsp;<!-- 淘宝数据导入 --></div>
			<div class='header'>
				交易提示
			</div>
			<div class='main'>
				<div style="font-weight:bolder;">您需要立即处理的交易订单：</div>
				<div style="color:#55AAFF;">
					订单提示：
					<span>待受理订单（<span style="color:red;"><?php echo (intval($shopInfo['details']['waitHandleOrderCnt'])); ?></span>）</span>&nbsp;&nbsp;&nbsp;&nbsp;
					<span>待发货订单（<span style="color:red;"><?php echo (intval($shopInfo['details']['waitSendOrderCnt'])); ?></span>）</span>&nbsp;&nbsp;&nbsp;&nbsp;
					<span>待结束订单（<span style="color:red;"><?php echo (intval($shopInfo['details']['appraisesOrderCnt'])); ?></span>）</span>
				</div>
			</div>
			<div class='current'>周订单量（<?php echo (intval($shopInfo['details']['weekOrderCnt'])); ?>）&nbsp;&nbsp;&nbsp;&nbsp;周交易金额（<?php echo (intval($shopInfo['details']['weekOrderMoney'])); ?>）&nbsp;&nbsp;&nbsp;&nbsp;一个月订单量（<?php echo (intval($shopInfo['details']['monthOrderCnt'])); ?>）&nbsp;&nbsp;&nbsp;&nbsp;一个月交易金额（<?php echo (intval($shopInfo['details']['monthOrderMoney'])); ?>）&nbsp;&nbsp;&nbsp;&nbsp;</div>
			
			<div class='header' style='display:none'>
				支付帐号
			</div>
			<div class='main' style='display:none'>
				<div style="float:left;width:100px;height:100px;">
					<img src="/Tpl/default/images/wst.jpg" width="100" height="100"/>
				</div>
				<div style="float:left;width:400px;height:100px;padding-left:10px;">
					您的帐户余额：43349元<br/>
					<button style="width:80px;height:30px;background-color:#e23e3d;color:#ffffff;border:1px solid #ffffff;">帐户充值</button><br/>
					<div style="color:#55AAFF;">
						<span>支付帐号</span>&nbsp;&nbsp;&nbsp;&nbsp;<span>提现</span>&nbsp;&nbsp;&nbsp;&nbsp;<span>出入明细</span>&nbsp;&nbsp;&nbsp;&nbsp;
					</div>
				</div>
				<div class="wst-clear"></div>
			</div>
			
		</div>
		
		<div style="width:192px;float:left;min-height:400px;border-left:1px solid #cccccc;">
			<div style="height:36px;line-height:36px;font-weight:bolder;padding-left:10px;">
				平台联系方式
			</div>
			<div style="padding-left:10px;line-height:26px;">
				<div>电话：<?php echo ($shopInfo['shop']['userPhone']); ?></div>
				<div>邮箱：<?php echo ($shopInfo['shop']['userEmail']); ?></div>
				<div>服务时间：<?php echo ($shopInfo['shop']['serviceStartTime']); ?>-<?php echo ($shopInfo['shop']['serviceEndTime']); ?></div>
			</div>
		</div>
		<div class="wst-clear"></div>
	</div>
</div>

            </div>
          </div>
          <div style='clear:both;'></div>
          <br/>
        </div>
    </body>
        <script src="/Public/plugins/formValidator/formValidator-4.1.3.js"></script>
      <script src="/Public/js/common.js"></script>
        <script src="/Tpl/default/js/shopcom.js"></script>
        <script src="/Tpl/default/js/head.js"></script>
        <script src="/Tpl/default/js/common.js"></script>
        <script src="/Public/plugins/layer/layer.min.js"></script>
    <script type="text/javascript">
      $(function(){
        <?php if(!$selected_menu): ?>$('.wst-menu-title').next('div').eq(0).slideDown();<?php endif; ?>
        $('.wst-menu-title').click(function(){
          if($(this).next('div').css('display') == 'block') {$(this).next('div').slideUp();return;}
          $('.wst-menu-title').next('div').slideUp();
          $(this).next('div').slideDown();
        });
      });
    </script>
    <style>.wst-menu-title{cursor:pointer;}</style>
</html>